Pod::Spec.new do |s|
  s.name         = 'SSZipArchive'
  s.version      = '2.0.6'
  s.summary      = 'Utility class for zipping and unzipping files on iOS.'
  s.description  = 'SSZipArchive is a simple utility class for zipping and unzipping files on iOS.'
  s.homepage     = 'https://bitbucket.org/tengpan/ziparchive'
  s.license      = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.authors      = { 'Sam Soffes' => 'sam@soff.es', 'Joshua Hudson' => nil, 'Antoine Cœur' => nil }
  s.source       = { :git => 'https://bitbucket.org/tengpan/ziparchive.git', :tag => "v#{s.version}" }
  s.ios.deployment_target = '8.0'
  s.source_files = 'SSZipArchive/*.{m,h}', 'SSZipArchive/minizip/*.{c,h}', 'SSZipArchive/minizip/aes/*.{c,h}'
  s.public_header_files = 'SSZipArchive/*.h'
  s.library = 'z'
end
